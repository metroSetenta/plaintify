import { NavLink } from "react-router-dom";
import TextBoldM from '../text/text-bold-m';
import styles from './playlist-button.module.css';

function PlaylistButton({ImgUrl, children, href}){
    return (
        <NavLink to={href}>
            <button className={styles.button}>
                <img src={ImgUrl} alt={ImgUrl} />
                <TextBoldM>{children}</TextBoldM>
            </button>
        </NavLink>
    );
}

export default PlaylistButton;