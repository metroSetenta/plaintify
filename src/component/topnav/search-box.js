import * as Icons from '../icons';
import styles from './search-box.module.css';

function SearchBox() {
    return (
        <div className={styles.SeachBox}>
            <Icons.Search />
            <input placeholder="Artistas, canciones o podcasts" maxLength="80"/>
        </div>
    );
}
  
export default SearchBox;