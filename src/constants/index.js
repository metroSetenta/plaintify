import * as Icon from '../component/icons'
import React from 'react'

export default {
  MOBILE_SIZE: 640,
}

export const MENU = [
  {
    title: 'Inicio',
    path: '/',
    icon: <Icon.Home />,
    iconSelected: <Icon.HomeActive />
  },
  {
    title: 'Buscar',
    path: '/search',
    icon: <Icon.Search />,
    iconSelected: <Icon.SearchActive />
  },
  {
    title: 'Biblioteca',
    path: '/library',
    icon: <Icon.Library />,
    iconSelected: <Icon.LibraryActive />
  }
]

export const PLAYLISTBTN = [
    {
      title: 'Crear lista o carpeta',
      path: '/',
      ImgUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjNbbzYhoC8PGU2FPO4YUk8Y_ZIOWKAHB8gg&usqp=CAU',
    },
    {
      title: 'Canciones que te gustan',
      path: '/',
      ImgUrl: 'https://misc.scdn.co/liked-songs/liked-songs-64.png',
    }
]

export const LIBRARYTABS = [
  {
    title: 'Listas de reproducción',
    path: '/library'
  },
  {
    title: 'Podcasts',
    path: '/library/podcasts'
  },
  {
    title: 'Artistas',
    path: '/library/artists'
  },
  {
    title: 'Álbumes',
    path: '/library/albums'
  }
]