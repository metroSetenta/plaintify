# 🎧️ Plaintify

This is a project to do a kata code about **Trunk base development**!


## 🥋 What is a code kata

In software development, a "code kata" is a term that comes from martial arts and has been adopted in the programming community to refer to a specific programming exercise designed to improve a developer's skills and proficiency. Code katas are deliberate, repetitive practices performed to perfect techniques, become familiar with a programming language, or learn to use a particular library or framework.

Some common characteristics of a code kata include:

1. **Focus** on a specific task: A code kata centers around a specific programming problem. It can involve algorithms, data structures, software design, or any other aspect of development.

2. Repetition: Developers practice the same code kata multiple times to enhance their understanding and ability to solve that particular problem.

3. Constraints: Often, specific constraints or rules are imposed during the code kata to encourage creativity and lateral thinking.

4. **Feedback**: After completing a code kata, it's common to review and discuss the code with other developers to get feedback and learn better approaches.

5. Continuous learning: Code katas are a tool for continuous learning and can be used by both beginner and experienced programmers.

Code katas are an excellent way to gain hands-on experience and improve programming skills, and they can also be used as a fun form of team training in the software development field. Furthermore, there are many code katas available online covering a wide range of topics and programming languages, so developers can choose the ones that best suit their learning and improvement needs.


## ⚒️ The specific task
(Taken from [Google Cloud Architecture Center](https://cloud.google.com/architecture/devops/devops-tech-trunk-based-development?hl=es-419))


## What is Trunk base development

Trunk-based development is a version control management practice where developers merge small, frequent updates to a core “trunk” or main branch. Since it streamlines merging and integration phases, it helps achieve CI/CD and increases software delivery and organizational performance.


## 👨‍💻 Available Scripts
npm used version was **8.1.2**

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


## 📚 More readings

1. [Atlassian development devops documentation](https://www.atlassian.com/continuous-delivery/continuous-integration/trunk-based-development)
1. [Trunk Based Development](https://trunkbaseddevelopment.com/)
1. [Feature flags in a React app with GitLab's feature toggles](https://jamesrwilliams.ca/posts/feature-flags-in-a-react-app-with-gitlabs-feature-toggles/)